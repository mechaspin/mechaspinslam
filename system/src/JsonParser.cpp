/*
  Author:   Jacob Morgan
  Date:     6/24/20
  Company:  Mechaspin
  Extra:
*/

#include "JsonParser.h"
#include "Wirelog.h"

#include <json/value.h>
#include <json/json.h>
#include <fstream>
#include <iostream>

namespace mechaspin{
namespace system {

JsonParser* JsonParser::instance = nullptr;

//  Constructor
JsonParser::JsonParser() {
  lidarParamsLoaded = false;
  robotParamsLoaded = false;
  pclParamsLoaded = false;

  parseLidarConfiguration();
  parseJsonRobotValues();
  parsePclConfiguration();
}

void JsonParser::parseJsonRobotValues() {
  if(hasLoadedRobotConfig()) {
    return;
  }

  Json::Value robotParameters;
  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;
  std::string jsonFileName = "robotConfiguration";
  JSONCPP_STRING error;

  std::string jsonPath = (std::string) PROJECTDIR + "/json/" + jsonFileName + ".json";
  std::ifstream robot_file(jsonPath, std::ifstream::binary);
  if (!parseFromStream(builder, robot_file, &robotParameters, &error)) {
    WIRELOG_ERROR("Issue parsing " + jsonFileName + ".json")
    std::cout << "error: " << error << std::endl;
    exit(EXIT_SUCCESS);
  }
  if (robotParameters.empty()) {
    WIRELOG_ERROR("json file " + jsonFileName + ".json not found")
    exit(EXIT_SUCCESS);
  }

  parametersForRobot.loopFrequency_Hz = robotParameters["system"]["loop_frequency_Hz"].asFloat();
  parametersForRobot.wirelog_level = robotParameters["system"]["wirelog_level"].asUInt();

  parametersForRobot.dims.wheelDiameter_m = robotParameters["robotDimensions"]["wheelDiameter_m"].asFloat();
  parametersForRobot.dims.trackWidth_m = robotParameters["robotDimensions"]["trackWidth_m"].asFloat();
  parametersForRobot.dims.width_m = robotParameters["robotDimensions"]["width_m"].asFloat();
  parametersForRobot.dims.length_m = robotParameters["robotDimensions"]["length_m"].asFloat();
  parametersForRobot.dims.height_m = robotParameters["robotDimensions"]["height_m"].asFloat();

  parametersForRobot.kinematic.tickCounts_p_rev = robotParameters["robotMotorParameters"]["tickCounts_p_rev"].asInt();
  parametersForRobot.kinematic.speedLimitLinear_mps = robotParameters["robotMotorParameters"]["speedLimitLinear_mps"].asFloat();
  parametersForRobot.kinematic.speedLimitAngular_radps =
      robotParameters["robotMotorParameters"]["speedLimitAngular_radps"].asFloat();
  parametersForRobot.kinematic.fastDeceleration_rot_pss =
      robotParameters["robotMotorParameters"]["fastDeceleration_rot_pss"].asFloat();
  parametersForRobot.kinematic.deceleration_rot_pss = robotParameters["robotMotorParameters"]["deceleration_rot_pss"].asFloat();
  parametersForRobot.kinematic.acceleration_rot_pss = robotParameters["robotMotorParameters"]["acceleration_rot_pss"].asFloat();

  robotParamsLoaded = true;
}

void JsonParser::parseLidarConfiguration() {
  if(hasLoadedLidarConfig()) {
    return;
  }

  Json::Value lidarParameters;
  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;
  std::string jsonFileName = "lidarConfiguration";
  JSONCPP_STRING error;

  std::string jsonPath = (std::string) PROJECTDIR + "/json/" + jsonFileName + ".json";
  std::ifstream robot_file(jsonPath, std::ifstream::binary);
  if (!parseFromStream(builder, robot_file, &lidarParameters, &error)) {
    WIRELOG_ERROR("Issue parsing " + jsonFileName + ".json")
    std::cout << "error: " << error << std::endl;
    exit(EXIT_SUCCESS);
  }
  if (lidarParameters.empty()) {
    WIRELOG_ERROR("json file " + jsonFileName + ".json not found")
    exit(EXIT_SUCCESS);
  }

  parametersForLidar.loopFrequency_Hz = lidarParameters["system"]["loop_frequency_Hz"].asFloat();
  parametersForLidar.minRange_m = lidarParameters["system"]["minRange_m"].asDouble();
  parametersForLidar.maxRange_m = lidarParameters["system"]["maxRange_m"].asDouble();
  parametersForLidar.minAngle_deg = lidarParameters["system"]["minAngle_deg"].asDouble();
  parametersForLidar.maxAngle_deg = lidarParameters["system"]["maxAngle_deg"].asDouble();
  parametersForLidar.spinFrequency_Hz = lidarParameters["lidarParameters"]["spinFrequency_Hz"].asFloat();
  parametersForLidar.fieldOfView_deg = lidarParameters["lidarParameters"]["fieldOfView_deg"].asFloat();
  parametersForLidar.direction_deg = lidarParameters["lidarParameters"]["direction_deg"].asFloat();
  parametersForLidar.useFrontOfLidar = lidarParameters["lidarParameters"]["useFrontOfLidar"].asBool();
  parametersForLidar.useBackOfLidar = lidarParameters["lidarParameters"]["useBackOfLidar"].asBool();
  parametersForLidar.computeNormalForFront = lidarParameters["lidarParameters"]["computeNormalForFront"].asBool();
  parametersForLidar.computeNormalForBack = lidarParameters["lidarParameters"]["computeNormalForBack"].asBool();
  parametersForLidar.echoFilter = lidarParameters["lidarParameters"]["echoFilter"].asUInt();
  parametersForLidar.scanFrequency_Hz = lidarParameters["lidarParameters"]["scanFrequency"].asUInt();
  parametersForLidar.scanResolution = lidarParameters["lidarParameters"]["scanResolution"].asUInt();
  parametersForLidar.dataFormat = lidarParameters["lidarParameters"]["dataFormat"].asBool();
  parametersForLidar.ipAddress = lidarParameters["lidarParameters"]["ipAddress"].asString();
  parametersForLidar.hostIP = lidarParameters["lidarParameters"]["hostIP"].asString();
  parametersForLidar.macAddress = lidarParameters["lidarParameters"]["macAddress"].asString();
  parametersForLidar.tcpPort = lidarParameters["lidarParameters"]["tcpPort"].asUInt();
  parametersForLidar.hostPort = lidarParameters["lidarParameters"]["hostPort"].asUInt();

  lidarParamsLoaded = true;
}

void JsonParser::parsePclConfiguration() {
  if(hasLoadedPclConfig()) {
    return;
  }

  Json::Value pclParameters;
  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;
  std::string jsonFileName = "pclVisualizerConfiguration";
  JSONCPP_STRING error;

  std::string jsonPath = (std::string) PROJECTDIR + "/json/" + jsonFileName + ".json";
  std::ifstream robot_file(jsonPath, std::ifstream::binary);
  if (!parseFromStream(builder, robot_file, &pclParameters, &error)) {
    WIRELOG_ERROR("Issue parsing " + jsonFileName + ".json")
    std::cout << "error: " << error << std::endl;
    exit(EXIT_SUCCESS);
  }
  if (pclParameters.empty()) {
    WIRELOG_ERROR("json file " + jsonFileName + ".json not found")
    exit(EXIT_SUCCESS);
  }

  parametersForPcl.loopFrequency_Hz = pclParameters["system"]["loop_frequency_Hz"].asFloat();
  parametersForPcl.color.red = pclParameters["visualizer"]["backGroundColor"]["red"].asUInt();
  parametersForPcl.color.green = pclParameters["visualizer"]["backGroundColor"]["green"].asUInt();
  parametersForPcl.color.blue = pclParameters["visualizer"]["backGroundColor"]["blue"].asUInt();
  parametersForPcl.scansPerCloud = pclParameters["visualizer"]["scansPerCloud"].asInt();
  parametersForPcl.pointCloud_ID = pclParameters["visualizer"]["pointCloud_ID"].asString();
  parametersForPcl.coordinateSystemScale = pclParameters["visualizer"]["coordinateSystemScale"].asDouble();
  parametersForPcl.visualizerPointSize = pclParameters["visualizer"]["visualizerPointSize"].asDouble();

  pclParamsLoaded = true;
}

bool JsonParser::hasLoadedRobotConfig() const {
  return robotParamsLoaded;
}

bool JsonParser::hasLoadedLidarConfig() const {
  return lidarParamsLoaded;
}

bool JsonParser::hasLoadedPclConfig() const {
  return pclParamsLoaded;
}

bool JsonParser::isConfigValid() const {
  return lidarParamsLoaded && robotParamsLoaded && pclParamsLoaded;
}

JsonParser *JsonParser::getInstance() {
  if (instance == nullptr) {
    WIRELOG("Creating new instance of json parser")
    instance = new JsonParser();
  }

  return instance;
}

void JsonParser::deleteInstance() {
  if (instance != nullptr) {
    delete instance;
    instance = nullptr;
    WIRELOG("Deleted instance of JsonParser")
  } else {
    WIRELOG_WARNING("Could not delete JsonParser instance since already nullptr")
  }
}

JsonParser::robotConfiguration::robotConfiguration() {
  loopFrequency_Hz = 31.25;
  wirelog_level = 5;

  dims.height_m = 0;
  dims.width_m = 0;
  dims.length_m = 0;
  dims.trackWidth_m = 0;
  dims.wheelDiameter_m = 0;

  kinematic.tickCounts_p_rev = 0;
  kinematic.acceleration_rot_pss = 0;
  kinematic.deceleration_rot_pss = 0;
  kinematic.fastDeceleration_rot_pss = 0;
  kinematic.speedLimitAngular_radps = 0;
  kinematic.speedLimitLinear_mps = 0;
}

JsonParser::lidarConfiguration::lidarConfiguration() {
  loopFrequency_Hz = 0;
  spinFrequency_Hz = 0;
  fieldOfView_deg = 0;
  direction_deg = 0;
  useFrontOfLidar = true;
  useBackOfLidar = true;
  computeNormalForFront = false;
  computeNormalForBack = false;
  echoFilter = 0;
  scanFrequency_Hz = 0;
  scanResolution = 0;
  dataFormat = false;
  tcpPort = 0;
  hostPort = 0;
  ipAddress = "";
  hostIP = "";
  macAddress = "";
  maxAngle_deg = 0;
  minAngle_deg = 0;
  maxRange_m = 0;
  minRange_m = 0;
}

JsonParser::pclConfiguration::pclConfiguration() {
  loopFrequency_Hz = 62.5;
  color.red = 0;
  color.green = 0;
  color.blue = 0;
  scansPerCloud = 4;
  pointCloud_ID = "cloud";
  coordinateSystemScale = 1.0;
  visualizerPointSize = 1.0;
}

} // end namespace system
} // end namespace mechaspin

