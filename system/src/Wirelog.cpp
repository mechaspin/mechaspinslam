/*
  Author:   Jacob Morgan
  Date:     06/03/21
  Company:  Mechaspin
  Extra:
*/

#include "Wirelog.h"
#include "basicStructs.h"

#include <cstdio>
#include <thread>

#define BEGINRED      "\033[31m"
#define BEGINGREEN    "\033[32m"
#define BEGINYELLOW   "\033[33m"
#define BEGINBLUE     "\033[34m"
#define BEGINPURPLE   "\033[35m"
#define BEGINCYAN     "\033[36m"
#define BEGINWHITE    "\033[37m"
#define BEGINDEFAULT  "\033[39m"
#define NORMALCONSOLE "\033[0m"

namespace mechaspin {
namespace system {

bool firstCall;
bool shutdownCalled;
bool isThreadRunning;
uint8_t wirelogSeverityLevel;
Wirelog* Wirelog::instance = nullptr;

Wirelog::Wirelog() {
  firstCall = true;
  shutdownCalled = false;
  isThreadRunning = false;
  wirelogSeverityLevel = 5;
  WIRELOG("Created wirelog")
}

Wirelog::~Wirelog() noexcept(true) = default;

void Wirelog::run() {
  isThreadRunning = true;
  while(running) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    if(shutdownCalled) {
      setRunning(false);
    }
  }
  isThreadRunning = false;
}

Wirelog *Wirelog::getInstance() {
  if (instance == nullptr) {
    instance = new Wirelog();
  }

  return instance;
}

void Wirelog::wirelogInit(uint8_t setWirelogSeverityLevel) {
  wirelogSeverityLevel = setWirelogSeverityLevel;
  auto tempPointer = Wirelog::getInstance();
  std::time_t systemTime = getSystemTime();
  if(tempPointer && firstCall) {
    firstCall = false;
      WIRELOG("Starting up Wirelog with Severity level " + std::to_string(wirelogSeverityLevel))
  } else if(tempPointer) {
      WIRELOG("Setting Wirelog Severity level to " + std::to_string(wirelogSeverityLevel))
  }
}

void Wirelog::deleteInstance() {
  if (instance != nullptr) {
    delete instance;
    instance = nullptr;
    WIRELOG("Deleted instance of wirelog")
  } else {
    WIRELOG_WARNING("Could not delete wirelog instance since none exist")
  }
}

void Wirelog::wirelogShutdown(uint16_t timeout_ms) {
  auto maxCounts = timeout_ms/10;
  shutdownCalled = true;

  uint8_t counter = 0;
  while(isThreadRunning && (counter < maxCounts || maxCounts == 0)) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    counter++;
  }

  deleteInstance();
}

std::time_t Wirelog::getSystemTime() {
  auto systemTime = std::chrono::system_clock::now();
  return std::chrono::system_clock::to_time_t(systemTime);
}

void Wirelog::createGeneralWirelog(const std::string& message) {
  if(wirelogSeverityLevel > wirelogLevelEnum::NONE) {
    std::time_t systemTime = getSystemTime();
    std::cout << BEGINYELLOW << std::ctime(&systemTime) << BEGINDEFAULT << message << std::endl;
  }
}

void Wirelog::createErrorWirelog(const std::string &message) {
  if(wirelogSeverityLevel >= wirelogLevelEnum::WIRELOGERROR) {
    std::time_t systemTime = getSystemTime();
    std::cout << BEGINYELLOW << std::ctime(&systemTime);
    std::cout <<  BEGINRED << "ERROR: " << message << BEGINDEFAULT << std::endl;
  }
}

void Wirelog::createDebugWirelog(const std::string &message) {
  if(wirelogSeverityLevel >= wirelogLevelEnum::WIRELOGDEBUG) {
    std::time_t systemTime = getSystemTime();
    std::cout << BEGINYELLOW << std::ctime(&systemTime);
    std::cout << BEGINDEFAULT << "DEBUG: " << message << std::endl;
  }
}

void Wirelog::createWarningWirelog(const std::string &message) {
  if(wirelogSeverityLevel >= wirelogLevelEnum::WIRELOGWARN) {
    std::time_t systemTime = getSystemTime();
    std::cout << BEGINYELLOW << std::ctime(&systemTime);
    std::cout << BEGINCYAN << "WARNING: " << message << BEGINDEFAULT << std::endl;
  }
}

} // end namespace system
} // end namespace mechaspin
