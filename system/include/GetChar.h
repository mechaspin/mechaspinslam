/*
  Author:   Jacob Morgan
  Date:     02/19/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_SYSTEM_INCLUDE_GETCHAR_H
#define MECHASPINSLAM_SYSTEM_INCLUDE_GETCHAR_H

namespace mechaspin {
namespace system {

unsigned char getChar(double timeout_ms);

} // end namespace system
} // end namespace mechaspin

#endif //MECHASPINSLAM_SYSTEM_INCLUDE_GETCHAR_H
