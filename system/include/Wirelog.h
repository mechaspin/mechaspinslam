/*
  Author:   Jacob Morgan
  Date:     06/03/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_SYSTEM_INCLUDE_WIRELOG_H
#define MECHASPINSLAM_SYSTEM_INCLUDE_WIRELOG_H

#include <iostream>
#include <chrono>
#include <ctime>
#include <cstring>

#include "asynchronous.h"

#define WIRELOG(X) mechaspin::system::Wirelog::createGeneralWirelog(X);
#define WIRELOG_ERROR(X) mechaspin::system::Wirelog::createErrorWirelog(X);
#define WIRELOG_DEBUG(X) mechaspin::system::Wirelog::createDebugWirelog(X);
#define WIRELOG_WARNING(X) mechaspin::system::Wirelog::createWarningWirelog(X);

namespace mechaspin {
namespace system {

class Wirelog : public system::asynchronous {
public:
  static std::time_t getSystemTime();
  static void wirelogInit(uint8_t setWirelogSeverityLevel);
  static void wirelogShutdown(uint16_t timeout_ms);
  static void createGeneralWirelog(const std::string& message);
  static void createErrorWirelog(const std::string& message);
  static void createDebugWirelog(const std::string& message);
  static void createWarningWirelog(const std::string& message);

private:
  // Constructor
  Wirelog();
  // De-Constructor
  ~Wirelog() noexcept(true) override;

  void run() override;
  static void deleteInstance();
  static Wirelog* getInstance();

  static Wirelog *instance;
};

} // end namespace system
} // end namespace mechaspin

#endif //MECHASPINSLAM_SYSTEM_INCLUDE_WIRELOG_H
