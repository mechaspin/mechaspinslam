/*
  Author:   Jacob Morgan
  Date:     5/13/20
  Company:  Mechaspin
  Extra:
    https://stackoverflow.com/questions/3756323/how-to-get-the-current-time-in-milliseconds-from-c-in-linux
*/

#ifndef MECHASPINSLAM_SYSTEM_INCLUDE_TIMEREVENT_H
#define MECHASPINSLAM_SYSTEM_INCLUDE_TIMEREVENT_H

#include <boost/asio/steady_timer.hpp>
#include <boost/make_shared.hpp>
#include <boost/asio/io_service.hpp>

namespace mechaspin {
namespace system {

class TimerEvent {
public:
  // Constructor
  TimerEvent();
  // De-Constructor
  ~TimerEvent();

  // public functions
  static void runBlockingTimer_ms(uint64_t duration);
  void runNonBlockingTimer_ms(uint64_t duration);
  void waitForTimerToFinish();
  bool stopTimer();
  static double getRunTime_ms();

  bool timerDone();

private:
  std::unique_ptr<boost::asio::steady_timer> timer_;
  std::unique_ptr<boost::asio::io_service> io;
  bool timer_ms_done;

};

} // end namespace system
} // end namespace mechaspin

#endif // MECHASPINSLAM_SYSTEM_INCLUDE_TIMEREVENT_H
