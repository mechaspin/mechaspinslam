/*
  Author:   Jacob Morgan
  Date:     6/24/20
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_SYSTEM_INCLUDE_JSONPARSER_H
#define MECHASPINSLAM_SYSTEM_INCLUDE_JSONPARSER_H

#include <string>

namespace mechaspin{
namespace system {

class JsonParser {
public:
  struct robotConfiguration {
    float loopFrequency_Hz;
    uint8_t wirelog_level;
    struct robotDimensions {
      float wheelDiameter_m;
      float trackWidth_m;
      float width_m;
      float length_m;
      float height_m;
    } dims{};
    struct robotMotorParameters {
      float speedLimitLinear_mps;
      float speedLimitAngular_radps;
      float fastDeceleration_rot_pss;
      float deceleration_rot_pss;
      float acceleration_rot_pss;
      int tickCounts_p_rev;
    } kinematic{};
    robotConfiguration();
  };

  struct lidarConfiguration {
    float loopFrequency_Hz;
    float spinFrequency_Hz;
    float fieldOfView_deg;
    float direction_deg;
    bool useFrontOfLidar;
    bool useBackOfLidar;
    bool computeNormalForFront;
    bool computeNormalForBack;
    unsigned int echoFilter;
    unsigned int scanFrequency_Hz;
    unsigned int scanResolution;
    bool dataFormat;
    std::string ipAddress;
    std::string hostIP;
    std::string macAddress;
    unsigned int tcpPort;
    unsigned int hostPort;
    double maxRange_m;
    double minRange_m;
    double maxAngle_deg;
    double minAngle_deg;
    lidarConfiguration();
  };

  struct pclConfiguration {
    float loopFrequency_Hz;
    struct backGroundColor {
      uint8_t red;
      uint8_t green;
      uint8_t blue;
    } color{};
    int scansPerCloud;
    std::string pointCloud_ID;
    double coordinateSystemScale;
    double visualizerPointSize;
    pclConfiguration();
  };

  bool hasLoadedRobotConfig() const;
  bool hasLoadedLidarConfig() const;
  bool hasLoadedPclConfig() const;
  bool isConfigValid() const;
  static JsonParser* getInstance();
  static void deleteInstance();

  lidarConfiguration parametersForLidar;
  robotConfiguration parametersForRobot;
  pclConfiguration parametersForPcl;

private:
  // Constructor
  JsonParser();

  // De-Constructor
  ~JsonParser() = default;

  void parseJsonRobotValues();
  void parseLidarConfiguration();
  void parsePclConfiguration();

  bool robotParamsLoaded;
  bool lidarParamsLoaded;
  bool pclParamsLoaded;

  static JsonParser *instance;
};

} // end namespace system
} //  end namespace mechaspin

#endif // MECHASPINSLAM_SYSTEM_INCLUDE_JSONPARSER_H
