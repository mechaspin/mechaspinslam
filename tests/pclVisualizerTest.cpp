/*
  Author:   Jacob Morgan
  Date:     03/02/21
  Company:  Mechaspin
  Extra:
*/

// /usr/include
#include <iostream>
#include <thread>
#include <csignal>

// local includes
#include "devices/HcWaymoLidar.h"
#include "JsonParser.h"
#include "PclVisualization.h"

#if defined(WIN32)
static const char ASCII_ESC = VK_ESCAPE;
#else
static const char ASCII_ESC = 27;
#endif
static const char ASCII_NUL = 0;

void shutdown();
void signalHandler(int signum);
void processKeyboardInput();
void setLidarConfig(mechaspin::system::JsonParser::lidarConfiguration &parameters);

static bool running;
static mechaspin::lidarMeasurements lidarMeasurement;
static mechaspin::lidarConfiguration config;
static std::unique_ptr<mechaspin::HcWaymoLidar> lidarDevice;
static std::unique_ptr<mechaspin::PclVisualization> pclVisualizer;

int main(int argc, char** argv) {

  running = true;
  signal(SIGTERM, signalHandler);

  mechaspin::system::JsonParser* configReader = nullptr;
  configReader = mechaspin::system::JsonParser::getInstance();
  if(!configReader->isConfigValid()) {
    shutdown();
  } else {
    setLidarConfig(configReader->parametersForLidar);
    pclVisualizer = std::make_unique<mechaspin::PclVisualization>();
  }

  while(running) {
    processKeyboardInput();
  }

  mechaspin::system::JsonParser::deleteInstance();
  configReader = nullptr;

  std::cout << "Exiting the application" << std::endl;
  return 0;
}

void shutdown() {
  std::cout << "Shutting down the application." << std::endl;
  if(lidarDevice) {
    while(lidarDevice->lidarRunning()) {
      lidarDevice->stopLidar();
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    lidarDevice.reset();
  }

  if(pclVisualizer) {
    pclVisualizer->setRunning(false);
    pclVisualizer->join(2);
    pclVisualizer.reset();
  }

  running = false;
}

void signalHandler(int signum) {
  std::cout << "Interrupt signal (" << signum << ") received." << std::endl;
  shutdown();
  exit(signum);
}

void processKeyboardInput() {
  std::cout << "\n'g' + enter gets the lidar data\n" <<
            "'s' + enter starts the lidar\n" <<
            "'esc' + enter exits the application" << std::endl;

  unsigned char userInput;
  std::cin >> userInput;
  switch(userInput) {
  case 'g': {
    if(lidarDevice == nullptr) {
      std::cout << "Must initialize the lidar first with 's' + enter" << std::endl;
      break;
    }

    if(!lidarDevice->lidarIsInit()) {
      std::cout << "Lidar device is not initialized" << std::endl;
      break;
    }

    lidarMeasurement = lidarDevice->getLidarData(1);
//      std::cout << "Valid Data = " << (lidarMeasurement.isValid ? "true" : "false") << std::endl;
    if(lidarMeasurement.isValid) {
      pclVisualizer->addLidarDataToPointCloud(lidarMeasurement);
    }
    break;
  }
  case 's': {
    lidarDevice = std::make_unique<mechaspin::HcWaymoLidar>(config);
    break;
  }
  case ASCII_ESC: {
    shutdown();
    break;
  }
  default: {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    break;
  }
  }
}

void setLidarConfig(mechaspin::system::JsonParser::lidarConfiguration &parameters) {
  config.macAddress = parameters.macAddress;
  config.ipAddress = parameters.ipAddress;
  config.direction_deg = parameters.direction_deg;
  config.fieldOfView_deg = parameters.fieldOfView_deg;
  config.spinFrequency_Hz = parameters.spinFrequency_Hz;

  unsigned int bit0 = parameters.useFrontOfLidar ? (unsigned int) 1 : (unsigned int) 0;
  unsigned int bit1 = parameters.useBackOfLidar ? (unsigned int) 1 : (unsigned int) 0;
  bit1 = bit1 << 1u;
  unsigned int sides = bit1 | bit0;
  config.sidesToUse = sides;

  bit0 = parameters.computeNormalForFront ? (unsigned int) 1 : (unsigned int) 0;
  bit1 = parameters.computeNormalForBack ? (unsigned int) 1 : (unsigned int) 0;
  bit1 << 1u;
  sides = bit1 | bit0;
  config.computeNormalsForSides = sides;
}
