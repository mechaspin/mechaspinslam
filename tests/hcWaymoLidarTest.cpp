/*
  Author:   Jacob Morgan
  Date:     02/16/21
  Company:  Mechaspin
  Extra:
    Based on example waymo laser bear example
*/

#include <iostream>
#include "honeycomb/honeycomb.h"

#ifndef PI
#define PI M_PI
#endif

#define radToDeg(rad)     rad*180/PI

int main(int argc, char** argv) {
  waymo::Status status;
  waymo::Honeycomb lidar;

  if ((status = lidar.Init()) != waymo::Status::kOk) {
    std::cerr << "Failed to initialize Honeycomb lidar: " << status
              << std::endl;
    return 1;
  }

  if ((status = lidar.Run()) != waymo::Status::kOk) {
    std::cerr << "Failed to run Honeycomb lidar: " << status << std::endl;
    return 1;
  }

  if (lidar.SetSpinFrequency(10.0) != waymo::Status::kOk ||
      lidar.SetFieldOfView(210, 0) != waymo::Status::kOk ||
      lidar.SetSides(waymo::Sides::kBothSides) != waymo::Status::kOk ||
      lidar.SetComputeNormals(waymo::Sides::kNone) != waymo::Status::kOk) {
    std::cerr << "Failed to set Honeycomb options." << std::endl;
    return 1;
  }


  std::cout << "index, x, y, z, theta" << std::endl;
  if (lidar.IsRunning()) {
    waymo::ScansPtr scans;
    if ((status = lidar.GetLidarData(waymo::kDurationForever, true, &scans)) != waymo::Status::kOk) {
      std::cerr << "Failed to get lidar data: " << status << std::endl;
      return 1;
    }

    int j = 0;
    int k = 0;
    while(k < 10) {
      for (const auto &scan : scans->GetScans()) {
        for (int i = 0; i < scan.NumShots(); i++) {
          waymo::Shot shot = scan.GetShot(i);
          if (!shot.NumReturns()) {
            continue;
          }

          auto strongestReturn = *shot.GetReturns(waymo::ReturnMode::kStrongestReturn).begin();
          auto beamAngles = shot.GetBeamAnglesInRadians();
          auto yaw = beamAngles.yaw_rad;

          waymo::Vec3d coord = strongestReturn.GetCoordinates();
          auto x = coord.x;
          auto y = coord.y;
          auto z = coord.z;

          std::cout << j << ".) " << x << ", " << y << ", " << z << ", " << radToDeg(yaw) << std::endl;
          j++;
        } // end for each shot
      } // end for each scan
      k++;
    }
  } else {
    std::cout << "Lidar is not running" << std::endl;
  }

  if ((status = lidar.Stop()) != waymo::Status::kOk) {
    std::cerr << "Failed to stop Honeycomb lidar: " << status << std::endl;
    return 1;
  }

  return 0;
}


