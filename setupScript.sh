#!/bin/bash

#if ethernet not working
#sudo touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf
#sudo systemctl restart NetworkManager

#https://programmersought.com/article/86753448893/
#https://discourse.vtk.org/t/installing-vtk-in-ubuntu-18-04/2147/3  check Elvis' comment


current_directory=$PWD
ARCH=$(uname -m)

# Check for dependencies
sudo apt-get update
sudo apt-get install lsb-release -y
sudo apt-get install -y \
    clang \
    google-mock \
    libcurl4-openssl-dev \
    libgflags-dev \
    libsuitesparse-dev \
    python-sphinx \
    libreadline-dev \
		libncurses5-dev \
		cmake-curses-gui \
    libcpprest-dev \
		g++ \
		doxygen \
		git \
		libpcap-dev \
		libjsoncpp-dev \
		build-essential \
		linux-libc-dev \
		cmake \
		cmake-qt-gui \
		libusb-1.0-0-dev \
		libusb-dev \
		libudev-dev \
		mpi-default-dev \
		openmpi-bin \
		openmpi-common \
		libvips \
		libvips-dev \
		libflann-dev \
		libeigen3-dev \
		libboost-all-dev \
		libqhull* \
		libgtest-dev \
		freeglut3-dev \
		pkg-config \
		libxmu-dev \
		libxi-dev \
		mono-complete \
		libphonon-dev \
		phonon-backend-gstreamer \
		openni2-utils \
		libpcap-dev \
		libopenni-dev \
		libopenni2-dev \
		libopenni-sensor-primesense-dev \
		pcl-tools \
		libavcodec-dev \
		libavformat-dev \
		libavutil-dev \
		libdouble-conversion-dev \
		libexpat1-dev \
		libfontconfig-dev \
		libfreetype6-dev \
		libgdal-dev \
		libglew-dev \
		libhdf5-dev \
		libjpeg-dev \
		liblz4-dev \
		liblzma-dev \
		libnetcdf-dev \
		libnetcdf-cxx-legacy-dev \
		libogg-dev \
		libpng-dev \
		libpython3-dev \
		libsqlite3-dev \
		libswscale-dev \
		libtheora-dev \
		libtiff-dev \
		libxml2-dev \
		libxt-dev \
		zlib1g-dev \
		software-properties-gtk \
		libgl1-mesa-dev \
		gcc \
		graphviz \
		gettext \
		bzr \
		*libclang* \
		libxcb-xinerama0-dev \
		'^libxcb.*-dev' \
		libx11-xcb-dev \
		libxrender-dev \
		libxkbcommon-dev \
		libxkbcommon-x11-dev \
    libx11-dev \
    libxext-dev \
    libxtst-dev \
    libxrender-dev \
    libxmu-dev \
    libxmuu-dev \
    libqt5x11extras5-dev \
    qttools5-dev
		#qt5-default
    #qtbase5-dev \

# It is recomended to not install these and instead install vtk and qt from source
#    openjdk-8-jdk \
#    openjdk-8-jre \
#    libvtk7-dev:i386 libvtk6-dev:i386 libvtk7-dev libvtk6-dev
#    libqt5opengl5-dev \

sudo apt autoremove

# Check if user specified a install directory for git repositories
if [ $# -eq 0 ]; then
  gitRepo=$current_directory
elif $# -gt 1; then
	echo "Only 1 input parameter is allowed"
	exit 1
else
	gitRepo=$0
fi

# Verify the install directory for git repos
getDirectory=true
while $getDirectory -eq true
do
	echo ""
	echo "$gitRepo"
	read -r -p "The above directory is correct for cloning new git repos? [Y/n]: " answer
	[[ $answer =~ [Yy] ]] &&
	{
		getDirectory=false
	} ||
	{
		read -r -p "Enter Directory for installing git projects: " gitRepo
	}
done

## Check eigen is installed
#dpkg -s libeigen3-dev > /dev/null 2>&1 ||
#{
#	sudo apt-get install libeigen3-dev
#} &&
#{
#	echo "eigen3 already installed"
#}

# Ask user if they want to install OpenNI2 for their system
read -r -p "Do you need OpenNI2 Installed on this system? [Y/n]: " answer
# shellcheck disable=SC2015
[[ "$answer" != "${answer#[Yy]}" ]] &&
{
  cd
  cd Downloads
	wget http://dl.orbbec3d.com/dist/openni2/OpenNI_2.3.0.63.zip
	unzip OpenNI_2.3.0.63.zip
	cd OpenNI_2.3.0.63/Linux

  case $ARCH in
  x86_64)
    cp -r OpenNI-Linux-x64-2.3.0.63 $gitRepo
    cd "$gitRepo"
    mv OpenNI-Linux-x64-2.3.0.63 OpenNI2
    ;;
  aarch64)
    cp -r OpenNI-Linux-Arm64-2.3.0.63 $gitRepo
    cd "$gitRepo"
    mv OpenNI-Linux-Arm64-2.3.0.63 OpenNI2
    ;;
  *)
    cp -r OpenNI-Linux-x64-2.3.0.63 $gitRepo
    cd "$gitRepo"
    mv OpenNI-Linux-x64-2.3.0.63 OpenNI2
    ;;
  esac

  cd OpenNI2
  chmod a+x install.sh
  ./install.sh

  cd
  if grep -Fxq "export MECHASPIN_OPENNI2=${gitRepo}/OpenNI2" .bashrc; then
    sudo echo "export MECHASPIN_OPENNI2=${gitRepo}/OpenNI2" >> .bashrc
  fi
  cd /etc
  if grep -Fxq "export MECHASPIN_OPENNI2=${gitRepo}/OpenNI2" environment; then
    sudo echo "export MECHASPIN_OPENNI2=${gitRepo}/OpenNI2" >> environment
  fi

  cd
  cd Downloads
  sudo rm -R OpenNI_2.3.0.63.zip && sudo rm -R OpenNI_2.3.0.63
  cd $current_directory
} ||
{
	echo "Skipping OpenNI2 Install"
}

cd "$gitRepo" || exit
[ ! -d "qt5" ] &&
{
	git clone git://code.qt.io/qt/qt5.git
	cd qt5 || exit
	git checkout 5.15.1
	git submodule init
	git submodule update --recursive
	mkdir build && cd build || exit
	../configure -developer-build -opensource -nomake examples -nomake tests
	make -j$(($(nproc) - 1))
	#only run sudo make install if configured without -developer-build
}

cd "gitRepo" || exit
[ ! -d "vtk" ] &&
{
	git clone https://gitlab.kitware.com/vtk/vtk.git vtk qt support
	cd vtk || exit
  git checkout v8.2.0
	git submodule init
	git submodule update --recursive
	mkdir build && cd build || exit
	cmake ..
	make -j$(($(nproc) - 1))
	sudo make install
}

#https://robotica.unileon.es/index.php?title=PCL/OpenNI_tutorial_1:_Installing_and_testing#Precompiled_PCL_for_Ubuntu
#https://pointclouds.org/
#https://pcl-tutorials.readthedocs.io/en/latest/compiling_pcl_posix.html

# In pcl-1.11 and later, in pcl file PCLConfig.cmake.in find line string(REGEX REPLACE "[.-]" "_" LIB ${LIB}) and
#		add line afterwards with string(REPLACE "LIBUSB_1_0" "LIBUSB-1.0" LIB ${LIB})
#		Then rerun cmake, make, make install in build folder of pcl repo
# Use ccmake . to check configs of pcl. Build without OpenNI and OpenNI2
cd "gitRepo" || exit
[ ! -d "pcl" ] &&
{
	git clone https://github.com/PointCloudLibrary/pcl.git
	cd pcl || exit
  git checkout pcl-1.11.1
	git submodule init
	git submodule update --recursive
	awk '1;/string\(REGEX REPLACE \"\[.-]" \"_" LIB \$\{LIB})/ {print "  string(REPLACE ""LIBUSB_1_0"" ""LIBUSB-1.0"" LIB ${LIB})"}' PCLConfig.cmake.in > test.txt
	rm PCLConfig.cmake.in
	mv test.txt PCLConfig.cmake.in
	mkdir build && cd build || exit
	cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_GPU=ON-DBUILD_apps=ON -DBUILD_examples=ON -DWITH_OPENNI=OFF -DWITH_OPENNI2=OFF ..
	make -j$(($(nproc) - 1))
	sudo make install
}
