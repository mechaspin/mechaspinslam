/*
  Author:   Jacob Morgan
  Date:     02/17/21
  Company:  Mechaspin
  Extra:
*/

#include "basicStructs.h"

#include <string>
#include <map>
#include <vector>

#ifndef MECHASPINSLAM_SRC_DEVICES_GENERICLIDARDEVICE_H
#define MECHASPINSLAM_SRC_DEVICES_GENERICLIDARDEVICE_H

namespace mechaspin {

class GenericLidarDevice {
public:

  // Constructor
  explicit GenericLidarDevice() {};
  // De-Constructor
  virtual ~GenericLidarDevice() = default;

  virtual lidarMeasurements getLidarData(double timeout_s) = 0;
  virtual double getScanResolution_rad() = 0;
  virtual double getTimeStamp_ms() = 0;
  virtual double getTimeStampDate_ms() = 0;
  virtual double getStartAngle_rad() = 0;
  virtual double getStopAngle_rad() = 0;
  virtual double getBeamIntervalTime_ms() = 0;
  virtual bool startLidar() = 0;
  virtual bool stopLidar() = 0;
  virtual bool lidarRunning() = 0;

private:
  virtual void setTimeStamp_ms(const uint32_t& timestamp_ms) = 0;
  virtual void setTimeStamp_ms(const double& timestamp_ms) = 0;
  virtual void setAngleResolution_rad(const int32_t &angular_beam_resolution) = 0;
  virtual void setAngleResolution_rad(const double &angular_beam_resolution) = 0;
  virtual void setScanFrequency_Hz(const uint16_t &scan_frequency_Hz) = 0;
  virtual void setScanFrequency_Hz(const double &scan_frequency_Hz) = 0;
  virtual void setScanFrequencyAndAngleResolution(const uint16_t& scanTime_ms, const int32_t& angular_beam_resolution_rad) = 0;
  virtual void setScanFrequencyAndAngleResolution(const double& scanTime_ms, const double& angular_beam_resolution_rad) = 0;
  virtual void setDataFormat() = 0;
  virtual void setFilters() = 0;

};
} // end namespace mechaspin

#endif //MECHASPINSLAM_SRC_DEVICES_GENERICLIDARDEVICE_H
