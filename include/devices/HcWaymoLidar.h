/*
  Author:   Jacob Morgan
  Date:     02/15/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_SENSORS_INCLUDE_HCWAYMOLIDAR_H
#define MECHASPINSLAM_SENSORS_INCLUDE_HCWAYMOLIDAR_H

// local include
#include "devices/GenericLidarDevice.h"

#ifndef PI
#define PI M_PI
#endif

#define degToRad(degree)  degree*PI/180
#define radToDeg(rad)     rad*180/PI
#define OFFSET            degToRad(90)

#define RAD360            2*PI
#define RAD180            PI

namespace mechaspin {

class HcWaymoLidar : public GenericLidarDevice {
public:
  // Constructor
  explicit HcWaymoLidar(lidarConfiguration &config_);
  // De-Constructor
  ~HcWaymoLidar() noexcept(true) override;

  lidarMeasurements getLidarData(double timeout_s) override;
  double getScanResolution_rad() override;
  double getTimeStamp_ms() override;
  double getTimeStampDate_ms() override;
  double getStartAngle_rad() override;
  double getStopAngle_rad() override;
  double getBeamIntervalTime_ms() override;
  bool startLidar() override;
  bool stopLidar() override;
  bool lidarRunning() override ;

  bool lidarIsInit() const {return isInit;}

private:
  double maxRange_m;
  double minRange_m;
  double maxAngle_deg;
  double minAngle_deg;
  double lastLidarTimeStamp_ms;
  bool isInit;
  lidarConfiguration config;

  void setTimeStamp_ms(const uint32_t& timestamp_ms)  override;
  void setTimeStamp_ms(const double& timestamp_ms)  override;
  void setAngleResolution_rad(const int32_t &angular_beam_resolution)  override;
  void setAngleResolution_rad(const double &angular_beam_resolution)  override;
  void setScanFrequency_Hz(const uint16_t &scan_frequency_Hz)  override;
  void setScanFrequency_Hz(const double &scan_frequency_Hz)  override;
  void setScanFrequencyAndAngleResolution(const uint16_t& scanTime_ms, const int32_t& angular_beam_resolution_rad)  override;
  void setScanFrequencyAndAngleResolution(const double& scanTime_ms, const double& angular_beam_resolution_rad)  override;
  void setDataFormat()  override;
  void setFilters()  override;
  bool initialize();
};

} // end namespace mechaspin


#endif //MECHASPINSLAM_SENSORS_INCLUDE_HCWAYMOLIDAR_H
