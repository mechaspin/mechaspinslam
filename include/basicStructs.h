/*
  Author:   Jacob Morgan
  Date:     03/02/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_INCLUDE_BASICSTRUCTS_H
#define MECHASPINSLAM_INCLUDE_BASICSTRUCTS_H

#include <vector>
#include <cstdio>
#include <cstdint>
#include <string>

namespace mechaspin {

struct lidarMeasurements {
  std::vector<double> range_m;
  std::vector<double> angle_yaw_rad;
  std::vector<double> angle_pitch_rad;
  std::vector<double> range_x_m;
  std::vector<double> range_y_m;
  std::vector<double> range_z_m;
  std::vector<double> intensity;
  std::vector<double> beamTimeStamp_ms;
  std::vector<uint8_t> status;

  double timeStamp_ms;
  double timeStampDate_ms;

  bool isValid;

  lidarMeasurements();
};

struct lidarConfiguration {
  std::string ipAddress;
  std::string macAddress;
  double spinFrequency_Hz;
  double fieldOfView_deg;
  double direction_deg;
  unsigned int sidesToUse;
  unsigned int computeNormalsForSides;
  double maxRange_m;
  double minRange_m;
  double maxAngle_deg;
  double minAngle_deg;
  lidarConfiguration();
};

enum wirelogLevelEnum {
  NONE,
  WIRELOGMIN,
  WIRELOGDEBUG,
  WIRELOGWARN,
  WIRELOGERROR,
  WIRELOGMAX
};

} // end namespace mechaspin

#endif //MECHASPINSLAM_INCLUDE_BASICSTRUCTS_H
