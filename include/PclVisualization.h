/*
  Author:   Jacob Morgan
  Date:     02/24/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_INCLUDE_PCLVISUALIZATION_H
#define MECHASPINSLAM_INCLUDE_PCLVISUALIZATION_H

#include "asynchronous.h"
#include "basicStructs.h"

namespace mechaspin {

class PclVisualization : public system::asynchronous  {
public:
  // Constructor
  PclVisualization();

  // De-Constructor
  ~PclVisualization() noexcept(true) override;

  void addLidarDataToPointCloud(lidarMeasurements &scanData);

private:
  double startTime_ms;
  bool dataReady;
  bool pointCloudExists;

  float loopTime_ms;
  float loopFrequency_Hz;

  struct backGroundColor {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
  } color{};

  void run() override;

};
} // end namespace mechaspin

#endif //MECHASPINSLAM_INCLUDE_PCLVISUALIZATION_H
