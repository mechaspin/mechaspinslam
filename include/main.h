/*
  Author:   Jacob Morgan
  Date:     02/25/21
  Company:  Mechaspin
  Extra:
*/

#ifndef MECHASPINSLAM_INCLUDE_MAIN_H
#define MECHASPINSLAM_INCLUDE_MAIN_H

namespace mainspace {

void shutdown();
void signalHandler(int signum);
mechaspin::lidarConfiguration setLidarConfig();
void updatePointCloud();

static bool running = false;
static bool messageDisplayed = false;
static mechaspin::system::JsonParser* configReader = nullptr;
static std::unique_ptr<mechaspin::HcWaymoLidar> lidarDevice = nullptr;
static std::unique_ptr<mechaspin::PclVisualization> pclVisualizer = nullptr;

} // end namespace mainspace

#endif //MECHASPINSLAM_INCLUDE_MAIN_H
