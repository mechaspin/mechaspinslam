include(FindPackageHandleStandardArgs)
add_library(LidarBear SHARED IMPORTED)

if(${ARCHITECTURE} STREQUAL "x86_64")
    set_target_properties(LidarBear PROPERTIES
            IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/hc_waymo/libhoneycomb_c_api_x86.so
            INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/hc_waymo/include
            )
else(${ARCHITECTURE} STREQUAL "x86_64")
    set_target_properties(LidarBear PROPERTIES
            IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/hc_waymo/libhoneycomb_c_api_arm64.so
            INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/hc_waymo/include
            )
endif(${ARCHITECTURE} STREQUAL "x86_64")

get_target_property(LidarBear_LIBRARY LidarBear IMPORTED_LOCATION)
message(STATUS "LidarBear library directory = " ${LidarBear_LIBRARY})

get_target_property(LidarBear_INCLUDE_DIR LidarBear INTERFACE_INCLUDE_DIRECTORIES)
message(STATUS "LidarBear include directory = " ${LidarBear_INCLUDE_DIR})