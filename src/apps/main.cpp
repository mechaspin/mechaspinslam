/*
  Author:   Jacob Morgan
  Date:     02/15/21
  Company:  Mechaspin
  Extra:
*/

// /usr/include
#include <iostream>
#include <thread>
#include <csignal>

// local includes
#include "devices/HcWaymoLidar.h"
#include "JsonParser.h"
#include "PclVisualization.h"
#include "TimerEvent.h"
#include "Wirelog.h"

#include "main.h"

int main(int argc, char** argv) {
  using namespace mainspace;
  mechaspin::lidarConfiguration config;

  // Set default wirelog level before getting configs
  mechaspin::system::Wirelog::wirelogInit(5);

  running = true;
  signal(SIGINT, mainspace::signalHandler);

  configReader = mechaspin::system::JsonParser::getInstance();
  if(!configReader->isConfigValid()) {
    mainspace::shutdown();
    return 0;
  }

  // Set wirelog level from configs
  mechaspin::system::Wirelog::wirelogInit(configReader->parametersForRobot.wirelog_level);

  config = setLidarConfig();
  pclVisualizer = std::make_unique<mechaspin::PclVisualization>();
  lidarDevice = std::make_unique<mechaspin::HcWaymoLidar>(config);
  mechaspin::system::TimerEvent timer;

  float loopFrequency_hz = configReader->parametersForRobot.loopFrequency_Hz;
  float loopTime_ms = 1000.0f / loopFrequency_hz;

  while(running) {
    if(timer.timerDone()) {
      timer.runNonBlockingTimer_ms(std::floor(loopTime_ms));
      updatePointCloud();
    }

    timer.waitForTimerToFinish();
  }

  shutdown();
  WIRELOG("Exiting the application")
  return 0;
}

void mainspace::shutdown() {
  WIRELOG("Shutting down the application.")
  if(lidarDevice) {
    while(lidarDevice->lidarRunning()) {
      lidarDevice->stopLidar();
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    lidarDevice.reset();
  }

  if(pclVisualizer) {
    pclVisualizer->setRunning(false);
    pclVisualizer->join(2);
    pclVisualizer.reset();
  }

  if(configReader) {
    mechaspin::system::JsonParser::deleteInstance();
    configReader = nullptr;
  }

  mechaspin::system::Wirelog::wirelogShutdown(1000);
}

void mainspace::signalHandler(int signum) {
  std::cout << "\nInterrupt signal (" << signum << ") received. Setting running to false" << std::endl;
  running = false;
}

mechaspin::lidarConfiguration mainspace::setLidarConfig() {
  auto parameters = configReader->parametersForLidar;
  mechaspin::lidarConfiguration returnConfiguration;
  returnConfiguration.macAddress = parameters.macAddress;
  returnConfiguration.ipAddress = parameters.ipAddress;
  returnConfiguration.direction_deg = parameters.direction_deg;
  returnConfiguration.fieldOfView_deg = parameters.fieldOfView_deg;
  returnConfiguration.spinFrequency_Hz = parameters.spinFrequency_Hz;
  returnConfiguration.maxAngle_deg = parameters.maxAngle_deg;
  returnConfiguration.minAngle_deg = parameters.minAngle_deg;
  returnConfiguration.maxRange_m = parameters.maxRange_m;
  returnConfiguration.minRange_m = parameters.minRange_m;

  unsigned int bit0 = parameters.useFrontOfLidar ? (unsigned int) 1 : (unsigned int) 0;
  unsigned int bit1 = parameters.useBackOfLidar ? (unsigned int) 1 : (unsigned int) 0;
  bit1 = bit1 << 1u;
  unsigned int sides = bit1 | bit0;
  returnConfiguration.sidesToUse = sides;

  bit0 = parameters.computeNormalForFront ? (unsigned int) 1 : (unsigned int) 0;
  bit1 = parameters.computeNormalForBack ? (unsigned int) 1 : (unsigned int) 0;
  bit1 << 1u;
  sides = bit1 | bit0;
  returnConfiguration.computeNormalsForSides = sides;

  return returnConfiguration;
}

void mainspace::updatePointCloud() {
  if(lidarDevice == nullptr) {
    if(!messageDisplayed) {
      WIRELOG_WARNING("Cannot update point cloud. Lidar is a nullptr.")
      messageDisplayed = true;
    }
    return;
  }

  if(!lidarDevice->lidarIsInit()) {
    if(!messageDisplayed) {
      WIRELOG_WARNING("Cannot update point cloud. Lidar device is not initialized")
      messageDisplayed = true;
    }
    return;
  }

  mechaspin::lidarMeasurements lidarMeasurement;
  lidarMeasurement = lidarDevice->getLidarData(1);

  if(lidarMeasurement.isValid) {
    pclVisualizer->addLidarDataToPointCloud(lidarMeasurement);
  }
}

