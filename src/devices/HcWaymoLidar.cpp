/*
  Author:   Jacob Morgan
  Date:     02/15/21
  Company:  Mechaspin
  Extra:
    Based on example waymo laser bear example
*/

// local include
#include "devices/HcWaymoLidar.h"
#include "honeycomb/honeycomb.h"
#include "Wirelog.h"

// /usr/includes
#include "iostream"
#include <thread>
#include <utility>

#define STRONGLIDAR

namespace mechaspin {

static waymo::Status status;
std::unique_ptr<waymo::Honeycomb> lidar = nullptr;

// Constructor
HcWaymoLidar::HcWaymoLidar(lidarConfiguration &config_) : config(std::move(config_)) {
  lastLidarTimeStamp_ms = 0;
  maxRange_m = 0;
  minRange_m = 0;
  maxAngle_deg = 0;
  minAngle_deg = 0;
  isInit = initialize();
}

// Deconstructor
HcWaymoLidar::~HcWaymoLidar() noexcept(true) {
  if(lidar) {
    bool askedToStop = false;
    while(lidar->IsRunning()) {
      if(!askedToStop) {
        if ((status = lidar->Stop()) != waymo::Status::kOk) {
          WIRELOG_ERROR("Failed to stop Honeycomb lidar: ")
        } else {
          askedToStop = true;
        }
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    lidar.reset();
  }
}

bool HcWaymoLidar::initialize() {
  lidar = std::make_unique<waymo::Honeycomb>();

  WIRELOG("Initializing lidar device")
  if ((status = lidar->Init(config.ipAddress, config.macAddress)) != waymo::Status::kOk) {
    WIRELOG_ERROR("Failed to initialize Honeycomb lidar: ") std::cout << status << std::endl;
    lidar.reset();
    return false;
  }
  WIRELOG("Lidar Device Found")

  waymo::Honeycomb::SystemInfo info;
  if(lidar->GetSystemInfo(&info) == waymo::Status::kOk) {
    WIRELOG("Connected to device at address " + info.ip_address +
              " with mac address " + info.mac_address)
  } else {
    WIRELOG_ERROR("Unable to get System Info")
    return false;
  }

  if (!startLidar()) {
    return false;
  }

  auto sides = (waymo::WL_Sides) config.sidesToUse;
  auto sidesNormal = (waymo::WL_Sides) config.computeNormalsForSides;

  if (lidar->SetSpinFrequency(config.spinFrequency_Hz) != waymo::Status::kOk ||
      lidar->SetFieldOfView(config.fieldOfView_deg, config.direction_deg) != waymo::Status::kOk ||
      lidar->SetSides(sides) != waymo::Status::kOk ||
      lidar->SetComputeNormals(sidesNormal) != waymo::Status::kOk) {
    WIRELOG_ERROR("Failed to set Honeycomb options.")
    return false;
  }

  minRange_m = config.minRange_m;
  maxRange_m = config.maxRange_m;
  minAngle_deg = config.minAngle_deg;
  maxAngle_deg = config.maxAngle_deg;

  return true;
}

lidarMeasurements HcWaymoLidar::getLidarData(double timeout_s) {
  lidarMeasurements lidarReturnData;
  lidarReturnData.isValid = true;

  if(lidar == nullptr) {
    lidarReturnData.isValid = false;
    WIRELOG_WARNING("Lidar has not been initialized")
    return lidarReturnData;
  }

  double lidarTimeout_s = timeout_s;
  if(timeout_s <= 0) {
    lidarTimeout_s = waymo::kDurationForever;
  }

  if (lidar->IsRunning()) {
    waymo::ScansPtr scans;
    if ((status = lidar->GetLidarData(lidarTimeout_s, true, &scans)) != waymo::Status::kOk) {
      WIRELOG_ERROR("Failed to get lidar data: ") std::cout << status << std::endl;
      lidarReturnData.isValid = false;
      return lidarReturnData;
    }

    lidarReturnData.timeStamp_ms = (*scans->EndScans()).Timestamp();

    // Check if actually received new lidar data
    if(lidarReturnData.timeStamp_ms <= lastLidarTimeStamp_ms) {
      lidarReturnData.isValid = false;
      return lidarReturnData;
    }

    lastLidarTimeStamp_ms = lidarReturnData.timeStamp_ms;

    for (const auto& scan : scans->GetScans()) {
      lidarReturnData.beamTimeStamp_ms.emplace_back(scan.Timestamp());
      for (int i = 0; i < scan.NumShots(); i++) {
        waymo::Shot shot = scan.GetShot(i);

#ifdef STRONGLIDAR
        if(!shot.NumReturns()) {
          continue;
        }

        auto strongestReturn = *shot.GetReturns(waymo::ReturnMode::kStrongestReturn).begin();
        if(strongestReturn.RangeInMeters() < minRange_m || strongestReturn.RangeInMeters() > maxRange_m) {
          continue;
        }
#endif
        auto beamAngles = shot.GetBeamAnglesInRadians();
        lidarReturnData.angle_pitch_rad.emplace_back(beamAngles.pitch_rad);
        lidarReturnData.angle_yaw_rad.emplace_back(beamAngles.yaw_rad);

#ifndef STRONGLIDAR
        for(const waymo::Return &ret : shot.GetReturns(waymo::ReturnMode::kStrongestReturn)) {
          lidarReturnData.range_m.emplace_back(ret.RangeInMeters());
          lidarReturnData.intensity.emplace_back(ret.Intensity());

          waymo::Vec3d coord = ret.GetCoordinates();
          lidarReturnData.range_x_m.emplace_back(coord.x);
          lidarReturnData.range_y_m.emplace_back(coord.y);
          lidarReturnData.range_z_m.emplace_back(coord.z);
        }
#else
        lidarReturnData.range_m.emplace_back(strongestReturn.RangeInMeters());
        lidarReturnData.intensity.emplace_back(strongestReturn.Intensity());

        waymo::Vec3d coord = strongestReturn.GetCoordinates();
        lidarReturnData.range_x_m.emplace_back(coord.x);
        lidarReturnData.range_y_m.emplace_back(coord.y);
        lidarReturnData.range_z_m.emplace_back(coord.z);
#endif
      } // end for each shot
    } // end for each scan
  } else {
    WIRELOG("Lidar is not running")
    lidarReturnData.isValid = false;
  }

  return lidarReturnData;
}

bool HcWaymoLidar::startLidar() {
  if(lidar == nullptr) {
    WIRELOG_ERROR("Failed to start uninitialized Honeycomb lidar")
    return false;
  }

  WIRELOG("Starting the Waymo Honey Comb Lidar Device")
  if ((status = lidar->Run()) != waymo::Status::kOk) {
    WIRELOG_ERROR("Failed to run Honeycomb lidar: ") std::cout << status << std::endl;
    return false;
  }

  return true;
}

bool HcWaymoLidar::stopLidar() {
  if(lidar == nullptr) {
    WIRELOG_ERROR("Failed to stop uninitialized Honeycomb lidar")
    return false;
  }

  WIRELOG("Stopping the Waymo Honey Comb Lidar Device")
  if ((status = lidar->Stop()) != waymo::Status::kOk) {
    WIRELOG_ERROR("Failed to stop Honeycomb lidar: ") std::cout << status << std::endl;
    return false;
  }

  return true;
}

bool HcWaymoLidar::lidarRunning() {
  if(lidar == nullptr) {
    return false;
  }

  return lidar->IsRunning();
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------
//---------------------------------------------------------------------
double HcWaymoLidar::getScanResolution_rad() {
  return 0;
}
double HcWaymoLidar::getTimeStamp_ms() {
  return 0;
}
double HcWaymoLidar::getTimeStampDate_ms() {
  return 0;
}
double HcWaymoLidar::getStartAngle_rad() {
  return 0;
}
double HcWaymoLidar::getStopAngle_rad() {
  return 0;
}
double HcWaymoLidar::getBeamIntervalTime_ms() {
  return 0;
}
void HcWaymoLidar::setTimeStamp_ms(const uint32_t &timestamp_ms) {

}
void HcWaymoLidar::setTimeStamp_ms(const double &timestamp_ms) {

}
void HcWaymoLidar::setAngleResolution_rad(const int32_t &angular_beam_resolution) {

}
void HcWaymoLidar::setAngleResolution_rad(const double &angular_beam_resolution) {

}
void HcWaymoLidar::setScanFrequency_Hz(const uint16_t &scan_frequency_Hz) {

}
void HcWaymoLidar::setScanFrequency_Hz(const double &scan_frequency_Hz) {

}
void HcWaymoLidar::setScanFrequencyAndAngleResolution(const uint16_t &scanTime_ms,
                                                      const int32_t &angular_beam_resolution_rad) {

}
void HcWaymoLidar::setScanFrequencyAndAngleResolution(const double &scanTime_ms,
                                                      const double &angular_beam_resolution_rad) {

}
void HcWaymoLidar::setDataFormat() {

}
void HcWaymoLidar::setFilters() {

}
} // end namespace mechaspin
