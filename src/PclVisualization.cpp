/*
  Author:   Jacob Morgan
  Date:     02/24/21
  Company:  Mechaspin
  Extra:
*/

// local includes
#include "PclVisualization.h"
#include "JsonParser.h"
#include "TimerEvent.h"
#include "Wirelog.h"

// /usr/include
#include <pcl/pcl_base.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>

#include <memory>

namespace mechaspin {

bool addPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& cloud);
void updatePointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& cloud);

static pcl::visualization::PCLVisualizer::Ptr viewer = nullptr;
static pcl::PointCloud<pcl::PointXYZ>::Ptr basic_cloud_ptr = nullptr;

static std::string cloud_ID;
static double coordinateSystemScale;
static double visualizerPointSize;
int scansPerCloud;
uint8_t scanCounts;

// Constructor
PclVisualization::PclVisualization() {
  startTime_ms = system::TimerEvent::getRunTime_ms();
  dataReady = false;
  pointCloudExists = false;

  mechaspin::system::JsonParser* configReader = nullptr;
  configReader = mechaspin::system::JsonParser::getInstance();
  if(!configReader->isConfigValid()) {
    WIRELOG_ERROR("pcl: Configuration load failed")
    return;
  }

  loopFrequency_Hz = configReader->parametersForPcl.loopFrequency_Hz;
  color.red = configReader->parametersForPcl.color.red;
  color.green = configReader->parametersForPcl.color.green;
  color.blue = configReader->parametersForPcl.color.blue;
  scansPerCloud = configReader->parametersForPcl.scansPerCloud;
  cloud_ID = configReader->parametersForPcl.pointCloud_ID;
  coordinateSystemScale = configReader->parametersForPcl.coordinateSystemScale;
  visualizerPointSize = configReader->parametersForPcl.visualizerPointSize;

  loopTime_ms = 1000.0f / loopFrequency_Hz;

  create("pcl");
  WIRELOG("PCL thread Created")
}

// Deconstructor
PclVisualization::~PclVisualization() noexcept(true) {
  WIRELOG("Deconstructing PclVisualization")
  if(isRunning()) {
    setRunning(false);
  }

  if(viewer) {
    WIRELOG("pcl: reseting the viewer")
    viewer.reset();
  }

  if(basic_cloud_ptr) {
    WIRELOG("pcl: reseting the basic cloud pointer")
    basic_cloud_ptr.reset();
  }

  WIRELOG("Deconsructed PclVisualization")
}

void PclVisualization::run() {
  mechaspin::system::TimerEvent timer;
  basic_cloud_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
  viewer = pcl::visualization::PCLVisualizer::Ptr(new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  while(running) {
//    double currentTime = system::TimerEvent::getRunTime_ms() - startTime_ms;
//    std::cout << "Loop Start time = " << currentTime << std::endl << std::endl;

    if(timer.timerDone()) {
      timer.runNonBlockingTimer_ms(std::floor(loopTime_ms));
      if(dataReady && !viewer->wasStopped()) {
        dataReady = false;
        if(pointCloudExists) {
          updatePointCloud(basic_cloud_ptr);
        } else {
          pointCloudExists = addPointCloud(basic_cloud_ptr);
        }

        viewer->spinOnce(std::floor(loopTime_ms));
      }
    }

    timer.waitForTimerToFinish();
  }

  viewer->close();
  WIRELOG("Leaving the run loop in PclVisualization")
}

void PclVisualization::addLidarDataToPointCloud(lidarMeasurements &scanData) {
  if(basic_cloud_ptr == nullptr) {
    WIRELOG_WARNING("point cloud pointer is nullptr. Cannot build point cloud structure")
    return;
  }

  if(dataReady) {
    return;                   // Means have not had a chance to update point cloud
  }

  if(!scanCounts) {           // clear the point cloud for new data
    basic_cloud_ptr->clear();
  }

  for (long i = 0; i < scanData.range_m.size(); i++) {
    pcl::PointXYZ basic_point;
    basic_point.x = scanData.range_x_m.at(i);
    basic_point.y = scanData.range_y_m.at(i);
    basic_point.z = scanData.range_z_m.at(i);
    basic_cloud_ptr->points.push_back(basic_point);
  }

  basic_cloud_ptr->width = basic_cloud_ptr->size ();
  basic_cloud_ptr->height = 1;

  scanCounts++;
  scanCounts%=scansPerCloud;

  if(!scanCounts) {
    dataReady = true;
  }
}

bool addPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& cloud) {
  if(viewer == nullptr) {
    WIRELOG_WARNING("Cannot add point cloud to visualizer because it is nullptr")
    return false;
  }

//  viewer->removeAllPointClouds();
//  viewer->removeAllCoordinateSystems();
//  viewer->removeAllShapes();
  viewer->addPointCloud<pcl::PointXYZ>(cloud, cloud_ID);
  viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, visualizerPointSize, cloud_ID);
  viewer->addCoordinateSystem(coordinateSystemScale);
  return true;
}

void updatePointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& cloud) {
  if(viewer == nullptr) {
    WIRELOG_WARNING("Cannot update visualizer because it is nullptr")
    return;
  }

  viewer->updatePointCloud(cloud, cloud_ID);
}

} // end namespace mechaspin
