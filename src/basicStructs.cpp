/*
  Author:   Jacob Morgan
  Date:     03/02/21
  Company:  Mechaspin
  Extra:
*/

#include "basicStructs.h"

namespace mechaspin {

lidarMeasurements::lidarMeasurements() {
  range_m = {};
  angle_yaw_rad = {};
  angle_pitch_rad = {};
  range_x_m = {};
  range_y_m = {};
  range_z_m = {};
  intensity = {};
  beamTimeStamp_ms = {};
  status = {};

  timeStamp_ms = 0;
  timeStampDate_ms = 0;

  isValid = false;
}

lidarConfiguration::lidarConfiguration() {
  ipAddress = "";
  macAddress = "";
  spinFrequency_Hz = 0;
  fieldOfView_deg = 0;
  direction_deg = 0;
  sidesToUse = 0;
  computeNormalsForSides = 0;
  maxAngle_deg = 0;
  minAngle_deg = 0;
  maxRange_m = 0;
  minRange_m = 0;
}



} // end namespace mechaspin
